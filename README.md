# Twitch Manager

## Beschreibung

Aktuell ist es nur ein kleiner Chatbot und wird dann wohl nach und nach mit dem erweitert, was ich f�r meine Streams noch brauche.  
Vielleicht kann ja dennoch jemand was als Basis gebrauchen.

## Einrichtung

Die Konfiguration erfolgt �ber die appsettings.json. ( Hier kann auch eine appsettings.local.json  genutzt werden )  

* ChatBotUsername: Der Login-Name des Bot-Accounts
* OAuthToken: Das OAuthToken f�r den Login. ( Siehe unten )
* Channels: Die Kan�le die der Bot joinen soll.
* Greetings: Wenn der Bot den Kanal joint, sagt er eine Begr��ung. Es wird zuf�llig einer aus dem Array genommen. Kann leer / null sein, dann gibt es keinen Gru�.
* ChatCommands: Kommandos, die der Chatter eingeben kann, auf die der Bot dann Antwortet.
    * Name: Rein informeller Name.
    * Command: Das Kommenado, was der Chatter mit  !  eingeben muss
    * Reply: Die Antwort des Bots in den Kanal.
    * TimeOut: Timeout f�r das Kommando

## OAuthToken

<https://dev.twitch.tv/docs/irc/authenticate-bot/#getting-an-access-token>

<https://id.twitch.tv/oauth2/authorize?response_type=token&client_id=XXXXXXXXXXXXXXXXXXX&redirect_uri=XXXXXXXXXXXXXXXXXXXXXXXX&scope=chat%3Aread+chat%3Aedit>
