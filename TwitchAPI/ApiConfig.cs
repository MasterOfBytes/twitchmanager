﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchAPI
{
	public sealed class ApiConfig
	{
		public required string ChatBotUsername { get; set; }

		/// <summary>
		/// Access Token gotten by OAuth.
		/// </summary>
		/// <remarks>https://dev.twitch.tv/docs/irc/authenticate-bot/#getting-an-access-token</remarks>
		public required string OAuthToken { get; set; }
	}
}
