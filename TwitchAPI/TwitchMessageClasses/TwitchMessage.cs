﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchAPI
{
	public class TwitchMessage
	{
		public Dictionary<string, string>? Tags { get; private set; }
		public TwitchMessageSource? Source { get; private set; }
		public TwitchMessageCommand? TwitchCommand { get; private set; }
		public string Parameters { get; private set; } = string.Empty;

		public string RawTags { get; private set; } = string.Empty;
		public string RawSource { get; private set; } = string.Empty;
		public string RawCommand { get; private set; } = string.Empty;
		public string RawParameters { get; private set; } = string.Empty;

		public string RawMessage { get; private set; } = string.Empty;

		public string BotCommand { get; private set; } = string.Empty;
		public string BotCommandParameters { get; private set; } = string.Empty;

		public static TwitchMessage? FromMessage(string message)
		{
			if (string.IsNullOrWhiteSpace(message))
			{
				return null;
			}

			var twitchMessage = new TwitchMessage() 
			{
				RawMessage = message,
			};

			// Based on https://dev.twitch.tv/docs/irc/example-parser/

			// The start index. Increments as we parse the IRC message.
			var idx = 0;
			var endIdx = 0;

			// If the message includes tags, get the tags component of the IRC message.
			if (message[idx] == '@')
			{
				// The message includes tags.
				endIdx = message.IndexOf(' ');
				twitchMessage.RawTags = message.Substring(1, endIdx);
				idx = endIdx + 1; // Should now point to source colon (:).
			}

			// Get the source component (nick and host) of the IRC message.
			// The idx should point to the source part; otherwise, it's a PING command.
			if (message[idx] == ':')
			{
				idx += 1;
				endIdx = message.IndexOf(' ', idx);
				twitchMessage.RawSource = message.Substring(idx, endIdx - idx);
				idx = endIdx + 1;  // Should point to the command part of the message.
			}

			// Get the command component of the IRC message.

			endIdx = message.IndexOf(':', idx);  // Looking for the parameters part of the message.
			if (endIdx == -1)
			{
				endIdx = message.Length;
			}

			twitchMessage.RawCommand = message[idx..endIdx].Trim();

			if (endIdx != message.Length)
			{
				idx = endIdx + 1;
				twitchMessage.RawParameters = message[idx..];
			}

			twitchMessage.TwitchCommand = TwitchMessageCommand.Parse(twitchMessage.RawCommand);

			// Only parse the rest of the components if it's a command
			// we care about; we ignore some messages.

			if (null == twitchMessage.TwitchCommand)
			{
				// Is null if it's a message we don't care about.
				return null;
			}
			else
			{
				if (!string.IsNullOrWhiteSpace(twitchMessage.RawTags))
				{
					twitchMessage.Tags = twitchMessage.RawTags.Split(';').ToDictionary(k => k.Split('=')[0], v => v.Split('=')[1]);
				}

				if (!string.IsNullOrWhiteSpace(twitchMessage.RawSource))
				{
					twitchMessage.Source = TwitchMessageSource.Parse(twitchMessage.RawSource);
				}

				twitchMessage.Parameters = twitchMessage.RawParameters;
				if (!string.IsNullOrWhiteSpace(twitchMessage.RawParameters) && twitchMessage.RawParameters[0] == '!')
				{
					// The user entered a bot command in the chat window.        
					var commandPart = twitchMessage.RawParameters.Substring(1).Trim();
					var paramStartIndex = commandPart.IndexOf(' ');
					
					if (paramStartIndex >= 0)
					{
						twitchMessage.BotCommand = commandPart[..paramStartIndex];
						twitchMessage.BotCommandParameters = commandPart[paramStartIndex..].Trim();
					}
					else
					{
						twitchMessage.BotCommand = commandPart;
					}
				}
			}

			return twitchMessage;
		}

	}
}
