﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchAPI
{
	public class TwitchMessageSource
	{
		public string Host { get; private set; } = string.Empty;
		public string Nick { get; private set; } = string.Empty;

		public static TwitchMessageSource Parse(string rawSource)
		{
			var sourceParts = rawSource.Split('!');

			return new TwitchMessageSource()
			{
				Nick = (sourceParts.Length == 2) ? sourceParts[0] : string.Empty,
				Host = (sourceParts.Length == 2) ? sourceParts[1] : sourceParts[0],
			};
		}

		public override string ToString() => $"Host: {Host}; Nick: {Nick}";
	}
}
