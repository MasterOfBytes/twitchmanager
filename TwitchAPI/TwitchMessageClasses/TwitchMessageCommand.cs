﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchAPI
{
	public class TwitchMessageCommand
	{
		public string Command { get; private set; } = string.Empty;
		public string Channel { get; private set; } = string.Empty;
		public bool? IsCapRequestEnabled { get; private set; } = null;

		internal static TwitchMessageCommand? Parse(string rawCommand)
		{
			// Based on https://dev.twitch.tv/docs/irc/example-parser/

			if (string.IsNullOrWhiteSpace(rawCommand))
			{
				return null;
			}

			var command = new TwitchMessageCommand();
			var commandParts = rawCommand.Split(' ');

			switch (commandParts[0])
			{
				case "JOIN":
				case "PART":
				case "NOTICE":
				case "CLEARCHAT":
				case "HOSTTARGET":
				case "PRIVMSG":
				case "USERSTATE":   // Included only if you request the /commands capability. But it has no meaning without also including the /tags capabilities.
				case "ROOMSTATE":   // Included only if you request the /commands capability. But it has no meaning without also including the /tags capabilities.
					command.Command = commandParts[0];
					command.Channel = commandParts[1];
					break;
				case "PING":
					command.Command = commandParts[0];
					break;

				case "CAP":
					command.Command = commandParts[0];
					command.IsCapRequestEnabled = (commandParts[2] == "ACK") ? true : false;
					break;
				case "GLOBALUSERSTATE":
					// Included only if you request the /commands capability.
					// But it has no meaning without also including the /tags capability.
					command.Command = commandParts[0];
					break;
				case "RECONNECT":
					// console.log('The Twitch IRC server is about to terminate the connection for maintenance.')
					command.Command = commandParts[0];
					break;
				case "421":
					//console.log(`Unsupported IRC command: ${ commandParts[2]}`)
					return null;
				case "001":  // Logged in (successfully authenticated). 
					command.Command = commandParts[0];
					command.Channel = commandParts[1];
					break;
				case "002":  // Ignoring all other numeric messages.
				case "003":
				case "004":
				case "353":  // Tells you who else is in the chat room you're joining.
				case "366":
				case "372":
				case "375":
				case "376":
					//console.log(`numeric message: ${ commandParts[0]}`)
					return null;
				default:
					//console.log(`\nUnexpected command: ${ commandParts[0]}\n`);
					return null;
			}

			return command;
		}

		public override string ToString() => $"{Command} ( {Channel} )";
	}
}
