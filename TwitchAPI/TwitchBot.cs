﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading.Tasks;
using Websocket.Client;

namespace TwitchAPI
{
	public class TwitchBot
	{
		private readonly WebsocketClient webSocket;
		private const string socketURL = "wss://irc-ws.chat.twitch.tv:443";
		private const string getOAuthTokenURL = "https://id.twitch.tv/oauth2/authorize?response_type=token&client_id=[CLIENTID]&redirect_uri=[REDIRECTURI]&scope=chat%3Aread+chat%3Aedit";
		//private const string socketURL = "wss://demo.piesocket.com/v3/channel_123?api_key=VCXCEuvhGcBDP7XhiJJUDvR1e1D3eiVjgZ9VRiaV";
		//private const string socketURL = "wss://ws.postman-echo.com/raw";
		//private const string socketURL = "wss://socketsbay.com/wss/v2/1/demo/";

		private readonly ILogger<TwitchBot> log;
		private readonly ApiConfig apiConfig;
		private readonly BotConfig config;

		public TwitchBot(ILogger<TwitchBot> log, IOptions<ApiConfig> apiConfig, IOptions<BotConfig> botConfig)
		{
			this.log = log;
			this.apiConfig = apiConfig.Value;
			this.config = botConfig.Value;

			webSocket = new WebsocketClient(new Uri(socketURL));
			webSocket.ReconnectTimeout = null;

			webSocket.ReconnectionHappened.Subscribe(SocketReconnected);
			webSocket.DisconnectionHappened.Subscribe(SocketDisconnected);
			webSocket.MessageReceived.Subscribe(SocketMessageReceived);
		}

		private async void SocketMessageReceived(ResponseMessage message)
		{
			log.LogDebug("Socketmessage received: {message}", message);

			try
			{
				if (message.MessageType != WebSocketMessageType.Text || string.IsNullOrWhiteSpace(message.Text))
				{
					return;
				}

				var messages = message.Text.Split("\r\n").Select(TwitchMessage.FromMessage).Where(m => m != null);
				foreach (var m in messages)
				{
					log.LogDebug("Message Received {message}", m);

					switch (m?.TwitchCommand?.Command)
					{
						case "001":
							foreach (var channelName in config.Channels)
							{
								await sendToSocketAsync($"JOIN #{channelName}");
								if (config.Greetings?.Length > 0)
								{
									await SendTextMessage(Random.Shared.GetItems(config.Greetings, 1).First(), channelName);
								}

							}
							break;
						case "353":
							log.LogDebug("353: {params}", m.Parameters);
							break;
						case "366":
							log.LogDebug("366: {params}", m.Parameters);
							break;
						case "JOIN":
							log.LogDebug("User Joined: {params}", m.Parameters);
							break;
						case "PRIVMSG":
							log.LogDebug("Received PRIVMSG: {params}", m.Parameters);
							await chatMessageReceivedAsync(m);
							break;
						case "PING":
							await sendToSocketAsync($"PONG {m.Parameters}");
							break;
					}
				}
			}
			catch (Exception ex)
			{
				log.LogError(ex, "Error while SocketMessageReceived: {message}", message);
			}
		}

		private void SocketDisconnected(DisconnectionInfo info)
		{
			log.LogInformation("Disconnection happened, type: {Info}", info);
		}

		private async void SocketReconnected(ReconnectionInfo info)
		{
			log.LogInformation("Reconnection happened, type: {Info}", info);

			await LoginAsync();
		}

		public async Task StartAsync()
		{
			log.LogDebug("Starting WebSocket {URL}", webSocket.Url);

			await webSocket.Start();

			log.LogInformation("Started WebSocket {URL}", webSocket.Url);
		}

		public async Task StopAsync()
		{
			log.LogDebug("Stopping WebSocket");

			await webSocket.Stop(WebSocketCloseStatus.NormalClosure, "Exiting");

			log.LogInformation("Stopped WebSocket");
		}

		private async Task LoginAsync()
		{
			try
			{
				await sendToSocketAsync($"CAP REQ :twitch.tv/membership twitch.tv/tags twitch.tv/commands");
				await sendToSocketAsync($"PASS oauth:{apiConfig.OAuthToken}");
				await sendToSocketAsync($"NICK {apiConfig.ChatBotUsername}");
			}
			catch (Exception ex)
			{
				log.LogError(ex, "Error while Login");
			}
		}

		private async Task SendTextMessage(string message, string channel) => await sendToSocketAsync($"PRIVMSG #{channel.TrimStart('#')} :{message}");
		private async Task sendToSocketAsync(string msg)
		{
			await webSocket.SendInstant(msg);

			if (msg.StartsWith("PASS"))
			{
				msg = msg[..msg.IndexOf(':')];
			}
			log.LogDebug("Sended {msg}", msg);
		}

		private async Task chatMessageReceivedAsync(TwitchMessage message)
		{
			if (!string.IsNullOrWhiteSpace(message.BotCommand) && !string.IsNullOrWhiteSpace(message?.TwitchCommand?.Channel))
			{
				var commandsToExecute = config.ChatCommands.Where(c => c.Command.Equals(message.BotCommand, StringComparison.CurrentCultureIgnoreCase));
				foreach (var command in commandsToExecute)
				{
					if (command.TimeOut == null || command.LastExecutionTime == null || (DateTime.Now - command.LastExecutionTime) > command.TimeOut)
					{
						await SendTextMessage(command.Reply, message.TwitchCommand.Channel);
						command.LastExecutionTime = DateTime.Now;
					}
				}
			}
		}

		public void Test()
		{
			//await Dispatcher.InvokeAsync(() => webSocket.StreamFakeMessage(ResponseMessage.TextMessage(":tmi.twitch.tv 001 masterofbytes :Welcome, GLHF!")));
			//await Dispatcher.InvokeAsync(() => webSocket.StreamFakeMessage(ResponseMessage.TextMessage(":tmi.twitch.tv 002 masterofbytes :Your host is tmi.twitch.tv")));
			//await Dispatcher.InvokeAsync(() => webSocket.StreamFakeMessage(ResponseMessage.TextMessage(":tmi.twitch.tv 003 masterofbytes :This server is rather new")));
			//await Dispatcher.InvokeAsync(() => webSocket.StreamFakeMessage(ResponseMessage.TextMessage(":tmi.twitch.tv 004 masterofbytes :-")));
			//await Dispatcher.InvokeAsync(() => webSocket.StreamFakeMessage(ResponseMessage.TextMessage(":tmi.twitch.tv 375 masterofbytes :-")));
			//await Dispatcher.InvokeAsync(() => webSocket.StreamFakeMessage(ResponseMessage.TextMessage(":tmi.twitch.tv 372 masterofbytes :You are in a maze of twisty passages, all alike.")));
			//await Dispatcher.InvokeAsync(() => webSocket.StreamFakeMessage(ResponseMessage.TextMessage(":tmi.twitch.tv 376 masterofbytes :>")));

			//await Dispatcher.InvokeAsync(() => webSocket.StreamFakeMessage(ResponseMessage.TextMessage(":masterofbytes!masterofbytes@masterofbytes.tmi.twitch.tv JOIN #masterofbytes")));
			//await Dispatcher.InvokeAsync(() => webSocket.StreamFakeMessage(ResponseMessage.TextMessage(":masterofbytes.tmi.twitch.tv 353 masterofbytes = #masterofbytes :masterofbytes")));
			//await Dispatcher.InvokeAsync(() => webSocket.StreamFakeMessage(ResponseMessage.TextMessage(":masterofbytes.tmi.twitch.tv 366 masterofbytes #masterofbytes :End of /NAMES list")));

			//await Dispatcher.InvokeAsync(() => webSocket.StreamFakeMessage(ResponseMessage.TextMessage(":masterofbytes!masterofbytes@masterofbytes.tmi.twitch.tv PRIVMSG #masterofbytes :sdf")));
			//await Dispatcher.InvokeAsync(() => webSocket.StreamFakeMessage(ResponseMessage.TextMessage(":masterofbytes!masterofbytes@masterofbytes.tmi.twitch.tv PRIVMSG #masterofbytes :!botcommand botparam")));
			//await Dispatcher.InvokeAsync(() => webSocket.StreamFakeMessage(ResponseMessage.TextMessage(":masterofbytes!masterofbytes@masterofbytes.tmi.twitch.tv PRIVMSG #masterofbytes :!was")));
			//await Dispatcher.InvokeAsync(() => webSocket.StreamFakeMessage(ResponseMessage.TextMessage(":masterofbytes!masterofbytes@masterofbytes.tmi.twitch.tv PRIVMSG #masterofbytes :!was @masterofbytes")));

			//await Dispatcher.InvokeAsync(() => webSocket.StreamFakeMessage(ResponseMessage.TextMessage("@badge-info=subscriber/41;badges=broadcaster/1,subscriber/0;client-nonce=4d870b1bcb90718ddefe47083efd63d3;color=;display-name=MasterOfBytes;emotes=emotesv2_9562b43767bd4fa1aab569476ab1e7d5:3-7/425618:24-26;first-msg=0;flags=;id=3c0a8342-409b-4373-ba60-29c749a07c57;mod=0;returning-chatter=0;room-id=48442732;subscriber=1;tmi-sent-ts=1701259914231;turbo=0;user-id=48442732;user-type= :masterofbytes!masterofbytes@masterofbytes.tmi.twitch.tv PRIVMSG #masterofbytes :Ok dnfHi  Geht dann los LUL  INJECTED!!\r\n")));


			/*

Message received: @badge-info=subscriber/41;badges=broadcaster/1,subscriber/0;color=;display-name=MasterOfBytes;emote-sets=0,300374282,301500754,360600536,368398342,462610930,477339272,592920959,344ad5cc-9890-4d83-a93d-e58f53e26da2;mod=0;subscriber=1;user-type= :tmi.twitch.tv USERSTATE #masterofbytes
@emote-only=0;followers-only=-1;r9k=0;room-id=48442732;slow=0;subs-only=0 :tmi.twitch.tv ROOMSTATE #masterofbytes

Sended PRIVMSG #masterofbytes :Kann losgehen!
Message received: @badge-info=subscriber/41;badges=broadcaster/1,subscriber/0;color=;display-name=MasterOfBytes;emote-sets=0,300374282,301500754,360600536,368398342,462610930,477339272,592920959,344ad5cc-9890-4d83-a93d-e58f53e26da2;id=d4b7d62e-6e9e-4177-9ac2-f6ad4ab73b56;mod=0;subscriber=1;user-type= :tmi.twitch.tv USERSTATE #masterofbytes

Message received: @badge-info=subscriber/41;badges=broadcaster/1,subscriber/0;client-nonce=4d870b1bcb90718ddefe47083efd63d3;color=;display-name=MasterOfBytes;emotes=emotesv2_9562b43767bd4fa1aab569476ab1e7d5:3-7/425618:24-26;first-msg=0;flags=;id=3c0a8342-409b-4373-ba60-29c749a07c57;mod=0;returning-chatter=0;room-id=48442732;subscriber=1;tmi-sent-ts=1701259914231;turbo=0;user-id=48442732;user-type= :masterofbytes!masterofbytes@masterofbytes.tmi.twitch.tv PRIVMSG #masterofbytes :Ok dnfHi  Geht dann los LUL

Received PRIVMSG: Ok dnfHi  Geht dann los LUL
Message received: :streamelements!streamelements@streamelements.tmi.twitch.tv JOIN #masterofbytes

Sended PRIVMSG #masterofbytes :Hallödri
Message received: @badge-info=subscriber/41;badges=broadcaster/1,subscriber/0;color=;display-name=MasterOfBytes;emote-sets=0,300374282,301500754,360600536,368398342,462610930,477339272,592920959,344ad5cc-9890-4d83-a93d-e58f53e26da2;id=47ab9fdf-5115-460d-85d1-e15c9138fc1f;mod=0;subscriber=1;user-type= :tmi.twitch.tv USERSTATE #masterofbytes

@room-id=48442732;
:masterofbytes!masterofbytes@masterofbytes.tmi.twitch.tv PRIVMSG #masterofbytes :!was
			*/
		}
	}
}
