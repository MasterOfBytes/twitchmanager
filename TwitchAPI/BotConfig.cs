﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchAPI
{
	public sealed class BotConfig
	{
		public required string[] Channels { get; set; }
		public required string[] Greetings { get; set; }
		public required List<ChatCommand> ChatCommands { get; set; }
	}
}
