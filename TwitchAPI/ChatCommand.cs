﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchAPI
{
	public class ChatCommand
	{
		public required string Name { get; set; }
		public required string Command { get; set; }
		public required string Reply {  get; set; }

		public DateTime? LastExecutionTime { get; set; }
		public TimeSpan? TimeOut { get; set; }
	}
}
