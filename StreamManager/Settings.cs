﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StreamManager
{
	internal class Settings
	{
		public string FileNameStreamNumber { get; set; } = "streamnumber.txt";
		public List<TextFile> TextFiles { get; set; } = [];




		public class TextFile
		{
			public string Filename { get; set; } = string.Empty;
		}
	}
}
