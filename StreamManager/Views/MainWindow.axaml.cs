﻿using Avalonia.Controls;
using StreamManager.ViewModels;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

namespace StreamManager.Views;

public partial class MainWindow : Window
{
	private readonly MainViewModel mainViewModel = new();
	private Settings? settings;
	private StreamStartTimer? streamStartTimer;
	
	private string filepath_todo => Path.Combine(mainViewModel.FilesFolder, "todo.txt");
	private string filepath_streamstart => Path.Combine(mainViewModel.FilesFolder, "streamstart.txt");



	public MainWindow()
	{
		InitializeComponent();

		DataContext = mainViewModel;

		Loaded += MainWindow_Loaded;
	}

	private async void MainWindow_Loaded(object? sender, Avalonia.Interactivity.RoutedEventArgs e)
	{
		try
		{
			var folder = new FileInfo(Assembly.GetExecutingAssembly().Location).Directory!.FullName;
			var json = await File.ReadAllTextAsync(Path.Combine(folder, "settings.json"));
			settings = System.Text.Json.JsonSerializer.Deserialize<Settings>(json);

			if (settings == null)
			{
				throw new System.Exception("Could not read settings.json");
			}

			if (File.Exists(filepath_todo))
			{
				mainViewModel.ToDo = await File.ReadAllTextAsync(filepath_todo);
			}
		}
		catch (System.Exception ex)
		{
			await new AlertBox("Load", ex.Message).ShowDialog(this);
		}
	}

	private void ButtonIncrementStreamNumber_Click(object? sender, Avalonia.Interactivity.RoutedEventArgs e)
	{
		mainViewModel.StreamNumber++;
	}

	private async void ButtonSave_Click(object? sender, Avalonia.Interactivity.RoutedEventArgs e)
	{
		if (sender is Button button)
		{
			button.IsEnabled = false;
		}

		try
		{
			await File.WriteAllTextAsync(filepath_todo, mainViewModel.ToDo);

		}
		catch (System.Exception ex)
		{
			await new AlertBox("Save", ex.Message).ShowDialog(this);
		}

		if (sender is Button button2)
		{
			button2.IsEnabled = true;
		}
	}

	private async void ButtonOpenFilesFolder_Click(object? sender, Avalonia.Interactivity.RoutedEventArgs e)
	{
		try
		{
			var filesFolder = mainViewModel.FilesFolder;
			if (!Directory.Exists(filesFolder))
			{
				Directory.CreateDirectory(filesFolder);
			}

			if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
			{
				Process.Start("explorer.exe", filesFolder);
			}
			else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
			{
				using Process dbusShowItemsProcess = new Process
				{
					StartInfo = new ProcessStartInfo
					{
						FileName = "dbus-send",
						Arguments = "--print-reply --dest=org.freedesktop.FileManager1 /org/freedesktop/FileManager1 org.freedesktop.FileManager1.ShowItems array:string:\"file://" + filesFolder.Replace("\\", "/") + "/\" string:\"\"",
						UseShellExecute = true
					}
				};
				dbusShowItemsProcess.Start();
				await dbusShowItemsProcess.WaitForExitAsync();

				if (dbusShowItemsProcess.ExitCode == 0)
				{
					// The dbus invocation can fail for a variety of reasons:
					// - dbus is not available
					// - no programs implement the service,
					// - ...
					return;
				}
			}
		}
		catch (System.Exception ex)
		{
			await new AlertBox("Open Files Folder", ex.Message).ShowDialog(this);
		}
	}

	private void ButtonStart_Click(object? sender, Avalonia.Interactivity.RoutedEventArgs e)
	{
		streamStartTimer = new(mainViewModel.StreamStartingDelay, filepath_streamstart);
		streamStartTimer.StartingInChanged += (s, ea) => mainViewModel.StartingIn = ea;
		streamStartTimer.Start();
	}

	private void ButtonStop_Click(object? sender, Avalonia.Interactivity.RoutedEventArgs e)
	{
		streamStartTimer?.Stop();
	}

	private void ButtonReset_Click(object? sender, Avalonia.Interactivity.RoutedEventArgs e)
	{
		streamStartTimer?.Reset();
	}
}
