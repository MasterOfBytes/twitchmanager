using Avalonia.Controls;

namespace StreamManager.Views
{
	public partial class AlertBox : Window
	{
		public string BoxTitle { get; set; } = string.Empty;
		public string BoxContent { get; set; } = string.Empty;

		public AlertBox()
		{
			InitializeComponent();
		}

		public AlertBox(string title, string content)
		{
			InitializeComponent();

			BoxTitle = title;
			BoxContent = content;

			DataContext = this;
		}

		private void Button_Click(object? sender, Avalonia.Interactivity.RoutedEventArgs e)
		{
			Close();
		}
	}
}
