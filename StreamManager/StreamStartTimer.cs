﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace StreamManager
{
	internal class StreamStartTimer
	{
		public TimeSpan Delay { get; }
		public string FileName { get; }
		public string? StartingInText => StartingIn?.ToString("mm\\:ss");

		public TimeSpan? StartingIn { get; private set; } = null;
		public event EventHandler<string?>? StartingInChanged;

		private readonly Timer tickTimer;

		public StreamStartTimer(TimeSpan delay, string fileName)
		{
			Delay = delay;
			FileName = fileName;
			tickTimer = new()
			{
				AutoReset = true,
				Interval = 1000,
			};
			tickTimer.Elapsed += TickTimer_Elapsed;
		}

		public void Start()
		{
			StartingIn ??= Delay;

			tickTimer.Start();
		}

		public void Reset()
		{
			StartingIn = Delay;
			File.WriteAllText(FileName, StartingInText);
			StartingInChanged?.Invoke(this, StartingInText);
		}

		public void Stop()
		{
			tickTimer.Stop();
		}

		private void TickTimer_Elapsed(object? sender, ElapsedEventArgs e)
		{
			try
			{
				StartingIn = StartingIn?.Subtract(TimeSpan.FromSeconds(1));

				File.WriteAllText(FileName, StartingInText);
				StartingInChanged?.Invoke(this, StartingInText);
			}
			catch (Exception)
			{
			}	
		}
	}
}
