﻿using CommunityToolkit.Mvvm.ComponentModel;
using System;
using System.IO;

namespace StreamManager.ViewModels;

public partial class MainViewModel : ObservableObject
{
	[ObservableProperty] 
	private int streamNumber = 4;

	[ObservableProperty]
	private string toDo = string.Empty;

	[ObservableProperty]
	private string? startingIn = null;

	public TimeSpan StreamStartingDelay { get; set; } = new TimeSpan(0, 10, 0);
	public string FilesFolder => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "TwitchManager");
}
