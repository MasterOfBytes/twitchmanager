﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using TwitchAPI;
using NLog.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace TwitchBotConsoleApp
{
	internal class Program
	{
		static async Task Main(string[] args)
		{
			var cfg = new ConfigurationBuilder()
				.AddJsonFile($"appsettings.json", true, false)
				.AddJsonFile($"appsettings.local.json", true, false)
				.AddEnvironmentVariables()
				.Build();



			var services = new ServiceCollection();

			services.AddLogging(configure => configure
				//.AddConsole()
				.AddNLog()
				.SetMinimumLevel(LogLevel.Trace)
			);

			services.Configure<ApiConfig>(cfg.GetSection(nameof(ApiConfig)));
			services.Configure<BotConfig>(cfg.GetSection(nameof(BotConfig)));

			services.AddTransient<TwitchBot>();
			var serviceProvider = services.BuildServiceProvider();

			var bot = serviceProvider.GetRequiredService<TwitchBot>();
			await bot.StartAsync();

			Console.WriteLine("Started. Press Enter to exit...");
			Console.ReadLine();

			await bot.StopAsync();
		}
	}
}